## Sample usage of stride-sidebar-card package

You can find it [here](https://bitbucket.org/atlassian/stride-sidebar-card)


### How to run
```
npm i
npm run build
```

And then open `index.html` from root of the project
